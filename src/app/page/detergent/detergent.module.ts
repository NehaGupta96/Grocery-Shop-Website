import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { FooterComponent } from 'src/app/component/footer/footer.component';
import { DetergentPageRoutingModule } from './detergent-routing.module';
import { HeaderComponent } from 'src/app/component/header/header.component';
import { DetergentPage } from './detergent.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetergentPageRoutingModule,HeaderComponent,FooterComponent
  ],
  declarations: [DetergentPage]
})
export class DetergentPageModule {}
