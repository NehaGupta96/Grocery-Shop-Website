import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ToiletPage } from './toilet.page';

describe('ToiletPage', () => {
  let component: ToiletPage;
  let fixture: ComponentFixture<ToiletPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ToiletPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
