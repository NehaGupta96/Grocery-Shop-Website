import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from 'src/app/component/footer/footer.component';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from 'src/app/component/header/header.component';
import { ToiletPageRoutingModule } from './toilet-routing.module';

import { ToiletPage } from './toilet.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ToiletPageRoutingModule,HeaderComponent,FooterComponent
  ],
  declarations: [ToiletPage]
})
export class ToiletPageModule {}
