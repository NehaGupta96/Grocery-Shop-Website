import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DetergentPowderPage } from './detergent-powder.page';

describe('DetergentPowderPage', () => {
  let component: DetergentPowderPage;
  let fixture: ComponentFixture<DetergentPowderPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(DetergentPowderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
