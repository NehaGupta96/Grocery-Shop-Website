import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetergentPowderPage } from './detergent-powder.page';

const routes: Routes = [
  {
    path: '',
    component: DetergentPowderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetergentPowderPageRoutingModule {}
