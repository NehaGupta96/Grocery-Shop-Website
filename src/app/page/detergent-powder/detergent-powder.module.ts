import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { FooterComponent } from 'src/app/component/footer/footer.component';
import { DetergentPowderPageRoutingModule } from './detergent-powder-routing.module';
import { HeaderComponent } from 'src/app/component/header/header.component';
import { DetergentPowderPage } from './detergent-powder.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetergentPowderPageRoutingModule,HeaderComponent,FooterComponent
  ],
  declarations: [DetergentPowderPage]
})
export class DetergentPowderPageModule {}
