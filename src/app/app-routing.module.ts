import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path:'',
    component:HeaderComponent
  },
  {
    path:'',
    component:FooterComponent
  },
  {
    path: 'detergent',
    loadChildren: () => import('./page/detergent/detergent.module').then( m => m.DetergentPageModule)
  },
  {
    path: 'detergent-powder',
    loadChildren: () => import('./page/detergent-powder/detergent-powder.module').then( m => m.DetergentPowderPageModule)
  },
  {
    path: 'toilet-cleaner',
    loadChildren: () => import('./page/toilet/toilet.module').then( m => m.ToiletPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
