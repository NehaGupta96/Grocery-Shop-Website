import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { HeaderComponent } from '../component/header/header.component';
import { HomePageRoutingModule } from './home-routing.module';
import { FooterComponent } from '../component/footer/footer.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,HeaderComponent,FooterComponent
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
